
    function ourServiceTabs() {
        const serviceTabs = document.querySelectorAll('.our-services-button')
        const tabsContent = document.querySelectorAll('.our-services-tabs-content')

        serviceTabs.forEach(function (item) {
            item.addEventListener('click', function () {
                let tabId = item.getAttribute('data-tab')
                let currentTab = document.querySelector(tabId)


                serviceTabs.forEach(function (item) {
                    item.classList.remove('active')
                })

                tabsContent.forEach(function (item) {
                    item.classList.remove('active')
                })

                item.classList.add('active')
                currentTab.classList.add('active')
            })
        })
    }


    function loadMoreFn() {
        const pictureArr = document.querySelector('.our-amazing-work-pictures');
        const loadMore = document.querySelector('.our-amazing-work-btn');
        const nonActiveCopy = document.querySelector('.our-work-img-div');
        const classes = ['web', 'graphic', 'landing', 'wordpress'];
        const loading = document.querySelector('.sec-loading')
        loadMore.addEventListener('click', function () {
            loading.classList.remove('no-btn')
            loadMore.remove();
            setTimeout(function () {
                for (let i = 0; i < 12; i++) {
                    const newCopy = nonActiveCopy.cloneNode(true);
                    const imgElement = newCopy.querySelector(".our-work-img");
                    nonActiveCopy.classList.remove('web');
                    const currentClass = classes[i % classes.length];
                    newCopy.classList.add(currentClass);
    
                    imgElement.src = `./img/news${i}`;
    
                    pictureArr.append(newCopy);
                }
                loading.classList.add('no-btn')
                filter();
            }, 1000);
   
        });
    }

    function filter() {
        const filterName = document.querySelectorAll('.our-work-filter');
        const filterItems = document.querySelectorAll('.our-work-img-div');
        const filterTabs = document.querySelectorAll('.our-work-filter-tabs')



        filterName.forEach(element => {
            element.addEventListener('click', event => {
                const targetId = event.target.dataset.id;
                const targetTab = event.target

                filterTabs.forEach(filterTab => filterTab.classList.remove('active'))
                targetTab.classList.add('active')
            
                switch (targetId) {
                    case '0':
                        filterItems.forEach(item => {
                            if (item.classList.contains('our-work-img-div')) {
                                item.style.display = 'flex'
                            } else {
                                item.style.display = 'none'
                            }
                        })
                        
                        break;
                    case 'web':
                        filterItems.forEach(item => {
                            if (item.classList.contains('web')) {
                                item.style.display = 'flex'
                            } else {
                                item.style.display = 'none'
                            }
                        })
                        break;
                    case 'graphic':
                        filterItems.forEach(item => {
                            if (item.classList.contains('graphic')) {
                                item.style.display = 'flex'
                            } else {
                                item.style.display = 'none'
                            }
                        })
                        break;
                    case 'landing':
                        filterItems.forEach(item => {
                            if (item.classList.contains('landing')) {
                                item.style.display = 'flex'
                            } else {
                                item.style.display = 'none'
                            }
                        })
                        break;
                    case 'wordpress':
                        filterItems.forEach(item => {
                            if (item.classList.contains('wordpress')) {
                                item.style.display = 'flex'
                            } else {
                                item.style.display = 'none'
                            }
                        })
                        break;
                }

            });
        });
    }
    filter();
    loadMoreFn()
    ourServiceTabs()