$(document).ready(function(){
    var $thumbnailImages = $('.thumbnail-slider').find('.thumbnail-img');
    var mainSlider = $('.slider-inner');
    var thumbnailSlider = $('.thumbnail-slider');
  
    mainSlider.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.thumbnail-slider'
    });
  
    thumbnailSlider.slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-inner',
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        infinite: true 
    });
  
    $('.thumbnail-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        $thumbnailImages.removeClass('active');
        $thumbnailImages.eq(nextSlide).addClass('active'); 
    });
    

    $('.next-arrow').on('click', function() {
        var activeIndex = $thumbnailImages.index($('.thumbnail-img.active'));
        var nextIndex = (activeIndex + 1) % $thumbnailImages.length;
        

        $thumbnailImages.removeClass('active');
        $thumbnailImages.eq(nextIndex).addClass('active');

        thumbnailSlider.slick('slickGoTo', nextIndex);
        

        mainSlider.slick('slickGoTo', nextIndex);
    });
    

    $('.prev-arrow').on('click', function() {
        var activeIndex = $thumbnailImages.index($('.thumbnail-img.active'));
        var prevIndex = (activeIndex - 1 + $thumbnailImages.length) % $thumbnailImages.length;
        

        $thumbnailImages.removeClass('active');
        $thumbnailImages.eq(prevIndex).addClass('active');
        
  
        thumbnailSlider.slick('slickGoTo', prevIndex);
        
    
        mainSlider.slick('slickGoTo', prevIndex);
    });
});
